package ru.alishev.springcourse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.alishev.springcourse.models.Item;
import ru.alishev.springcourse.models.Person;
import ru.alishev.springcourse.repositories.ItemsRepository;

import java.util.List;

@Service
public class ItemService {

    private final ItemsRepository itemRepository;

    @Autowired
    public ItemService(ItemsRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public List<Item> findByItemName(String itemName){
        return itemRepository.findByItemName(itemName);
    }

    public List<Item> findByOwner (Person owner){
        return itemRepository.findByOwner(owner);
    }


}
